<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="/register-css/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<title>Estiquent Register</title>
<!------ Include the above in your HEAD tag ---------->
<div class="register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome To
                        </h3>
                        <center><img class="tes" src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
                        <p>You are 30 seconds away from ease of managing transportation costs!</p>
                        <a href="/" style="margin-left:70px">Login</a><br/>
                    </div>
                    {{-- <form action="" id="registerForm"> --}}
                    <div class="col-md-9 register-right">
                        {{-- <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Employee</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Hirer</a>
                            </li>
                        </ul> --}}
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">Apply my Company</h3>
                                <form onsubmit="addCompany('file text', 'myfilename.txt', 'text/plain'); return false;">
                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""><b>Company Name</b></label>
                                            <input type="text" id="company_name" required class="form-control" placeholder="Company Name *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Short Name(It's For Your Company Code)</b></label>
                                            <input type="text" id="short_name" required class="form-control" placeholder="Just One Words,Don't write 'PT.') *" value="" />
                                            <input type="button" class="btnSorry" data-toggle="modal" data-target="#idont" value="I don't understand this"/>
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Company Address</b></label>
                                            <textarea name="address" id="address" required class="form-control" cols="30" rows="10" placeholder="Company Address"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""><b>Description</b></label>
                                            <textarea name="description" id="description" required class="form-control" cols="30" rows="10" placeholder="Tell us about your amazing company!"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Company Email</b></label>
                                            <input type="email" id="email" required class="form-control" placeholder="Or email of responsible person" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label for=""><b>Company Logo</b></label>
                                            <input type="file" minlength="10" id="logo" maxlength="10" name="logo" required class="form-control" placeholder="Your Phone *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" required name="vehicle1" value="Bike"> I agree with all terms and conditions<br>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <input type="submit" class="btnRegister" value="Register"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="idont" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">About Short Name Company</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p color="orange">
                                            Example,if your company name is "PT.Sukarasa Berjaya Makmur" then 
                                            you can put "Sukarasa" as your short name company.
                                        </p>
                                        <p>
                                            *this is needed for your company code and default username-password for
                                            superadmin
                                        </p>        
                                    </div>
                                </div>
                            </div>     
                        </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  
<script async="" src="https://cdn.rawgit.com/eligrey/FileSaver.js/e9d941381475b5df8b7d7691013401e171014e89/FileSaver.min.js"></script>
<script language="javascript">
//   $(document).ready(function () {
   
        function addCompany(text, name, type){
            console.log('ae');
            var company_name        = $('#company_name').val();
            var short_name          = $('#short_name').val();
            var address             = $('#address').val();
            var description         = $('#description').val();
            var email               = $('#email').val();
            console.log(company_name);

            $.post("http://localhost:8000/api/insert-company", {
                company_name: company_name,
                short_name: short_name,
                address: address,
                description: description,
                email: email
            }, function(data, status){
                console.log(data,data.kodok)
                if (status) {
                    toastr.success('Yeay! your company has signed up!')
                    // toastr.info('Please check your company email!')
                    var cc = data.cc
                    var text = `Your default Username is ${cc} // Your default Password is ${cc} // Use that Username & Password as Superadmin Login.`
                    var filename = 'Estiquent-Default Username-Password'
                    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
                    saveAs(blob, filename+".txt");
                    window.location.href=('/')
                }
                else {
                    toastr.danger('Insert Data Failed!')
                }
            });
        }
         

    </script>         