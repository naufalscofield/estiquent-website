@extends('layouts.master')

@section('title','Blog Codeffee')

@section('content')
  <h1>Halo Bray Codeffee</h1>
  <p>Codeffee gitu loh!</p>

  <hr>

  @foreach($blogs as $blog)
    <li> <a href="/laravel/public/blog/{{$blog->id}}"> {{ $blog->title }} </a>
      <form action="/laravel/public/blog/{{$blog->id}}" method="post">
        <input type="submit" name="submit" value="Delete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
      </form>
    </li>
  @endforeach

@endsection
