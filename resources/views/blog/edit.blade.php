@extends('layouts.master')

@section('title','Blog Codeffee Single')

@section('content')

  <h1>Halaman Edit</h1>

  <form action="/laravel/public/blog/{{$blog->id}}" method="post">
    <input type="text" name="title" value="{{ $blog->title }}"><br>
    <textarea name="description" rows="8" cols="80">{{ $blog->description }}</textarea><br>

    <input type="submit" name="submit" value="Edit">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
  </form>

@endsection
