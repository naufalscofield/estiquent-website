@extends('layouts.master')

@section('title','Blog Codeffee Single')

@section('content')

  <h1>Create Blog</h1>

  <form action="/laravel/public/blog" method="post">

    @if ($errors->has('title'))
      <p> {{ $errors->first('title') }} </p>
    @endif
    <input type="text" name="title" value="{{ old('title') }}"><br>

    @if ($errors->has('description'))
      <p> {{ $errors->first('description') }} </p>
    @endif
    <textarea name="description" rows="8" cols="80">{{ old('description') }}</textarea><br>

    <input type="submit" name="submit" value="Create">

    {{ csrf_field() }}
  </form>

@endsection
