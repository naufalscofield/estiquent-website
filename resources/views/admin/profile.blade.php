@extends('admin/master')

@section('title','Estiquent')
    
@section('content')

<div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
            </a>
        </div>
    {{-- <input type="text" value="{{ $role }}"> --}}
<?php
$r = $role; 
if ($r == 'admin') { ?>
        <ul class="nav">
            <li>
                <a href="/admin">
                    <i class="fa fa-tachometer"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="active">
                <a href="/profile">
                    <i class="fa fa-user"></i>
                    <p>Profile</p>
                </a>
            </li>
            <li>
                <a href="/users">
                    <i class="fa fa-users"></i>
                    <p>Users</p>
                </a>
            </li>
            <li>
                <a href="/warehouses">
                    <i class="fa fa-home"></i>
                    <p>Warehouses</p>
                </a>
            </li>
            <li>
                <a href="/destinations">
                    <i class="fa fa-map-marker"></i>
                    <p>Destinations</p>
                </a>
            </li>
            <li>
                <a href="/transactions">
                    <i class="fa fa-exchange"></i>
                    <p>Transactions</p>
                </a>
            </li>
            <li>
                <a href="/costs">
                    <i class="fa fa-usd"></i>
                    <p>Costs</p>
                </a>
            </li>
            <li>
                <a href="/company">
                    <i class="fa fa-building"></i>
                    <p>MyCompany</p>
                </a>
            </li>
            <li>
                <a href="/logs">
                    <i class="fa fa-history"></i>
                    <p>Logs</p>
                </a>
            </li>
            <li>
                <a href="/nwc">
                    <i class="fa fa-sort-numeric-asc"></i>
                    <p>nwc</p>
                </a>
            </li>
        </ul>
    <?php }else{ ?>
        <ul class="nav">
            <li class="active">
                <a href="/profile">
                    <i class="fa fa-user"></i>
                    <p>Profile</p>
                </a>
            </li>
            <li>
                <a href="/nwc">
                    <i class="fa fa-sort-numeric-asc"></i>
                    <p>nwc</p>
                </a>
            </li>
        </ul>
    <?php } ?>
    </div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">User Profile</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/logout">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="card card-user">
                        <div class="image">
                            <img src="/css/admin/img/background.jpg" alt="..."/>
                        </div>
                        <div class="content">
                            <div class="author">
                              <img class="avatar border-white" src="/css/admin/img/faces/face-2.jpg" alt="..."/>
                              <h4 class="title" id="name">
                              </h4>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Profile</h4>
                        </div>
                        <div class="content">
                            <form id="updateProfile">
                                <input type="hidden" id="id_user" class="form-control border-input" disabled placeholder="Company" value="{{ $id }}">    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" id="company_name">
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id="first_name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="last_name">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id="birth_place">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="birth_date">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id="username">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="password">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id="role">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" id="gender">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="text-center">
                                        {{ csrf_field() }}
                                    </div> --}}
                                <div class="text-center">
                                    <button type="button" id="update" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>

    function getProfile() {
        var  id = $('#id_user').val();
            console.log(id)
            $.get("http://localhost:8000/api/profile/" + id, function(data, status){
            console.log(data, status)
            if (status) {
                data.forEach(element => {
                // $('#id').html("")
                //     $('#id').append("<input type='text' required disabled id='id' class='form-control border-input' placeholder='Id' value="+id+">");
                $('#name').html("")
                    $('#name').append(""+element.first_name+" "+element.last_name+"</br><p class='description text-center'>"+element.role+"");
                $('#company_name').html("")
                    $('#company_name').append("<label>Company</label><input required type='text' class='form-control border-input' disabled placeholder='Company' value="+element.company_name+">");
                $('#first_name').html("")
                    $('#first_name').append("<label>First Name</label><input required type='text' id='first_name' class='form-control border-input' placeholder='First Name' value="+element.first_name+">");
                $('#last_name').html("")
                    $('#last_name').append("<label>Last Name</label><input required type='text' id='last_name' class='form-control border-input' placeholder='Last Name' value="+element.last_name+">");
                $('#birth_place').html("")
                    $('#birth_place').append("<label>Birth Place</label><input required type='text' id='birth_place' class='form-control border-input' placeholder='Birth Place' value="+element.birth_place+">");
                $('#birth_date').html("")
                    $('#birth_date').append("<label>Birth Date</label><input required type='date' id='birth_date' class='form-control border-input' placeholder='Birth Date' value="+element.birth_date+">");
                $('#username').html("")
                    $('#username').append("<label>Username</label><input required type='text' id='username' class='form-control border-input' placeholder='Username' value="+element.username+">");
                $('#password').html("")
                    $('#password').append("<label>Password</label><input required type='password' id='password' class='form-control border-input' placeholder='Password' value='{{ $pw }}'>");
                $('#role').html("")
                    $('#role').append("<label>Role</label><input required disabled type='text' id='role' class='form-control border-input' placeholder='Role' value="+element.role+">");
                $('#email').html("")
                    $('#email').append("<label>Email</label><input required type='email' id='email' class='form-control border-input' placeholder='Email' value="+element.email+">");
                // $('#gender').html("")
                //     $('#gender').append("<label>Gender</label><input class='form-control border-input' id='gender' required type='radio' name='gender' value='male'>Male<input class='form-control border-input' id='gender' required type='radio' name='gender' value='female'>Female");
                });
            }
            else {
                toastr.error('Load Data Failed!')
                }
            });
        }

       
        $(document).ready(function() {

            getProfile()

            
            $(document).on('click', '#update' ,function(){
                var id               = $('#id_user').val();
                var first_name       = $('#first_name').val();
                var last_name        = $('#last_name').val();
                var birth_place      = $('#birth_place').val();
                var birth_date       = $('#birth_date').val();
                var username         = $('#username').val();
                var password         = $('#password').val();
                var email            = $('#email').val();
    
                var data = {
                    id : id,
                    first_name : first_name,
                    last_name : last_name,
                    birth_place : birth_place,
                    birth_date : birth_date,
                    username : username,
                    password : password,
                    email : email,
                }

                console.log(data)

                jQuery.ajax({
                url: 'http://localhost:8000/api/update-profile/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    // getProfile();
                    toastr.success('Your Profile Has Been Updated!')
                    toastr.info('Please Relog To Confirm Your New Profile!')
                    window.location.href=('/logout')
                    }
                });
            });
        });
</script>    
@endsection