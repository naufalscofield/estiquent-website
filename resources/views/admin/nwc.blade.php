@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="/admin">
                        <i class="fa fa-tachometer"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/profile">
                        <i class="fa fa-user"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li>
                    <a href="/users">
                        <i class="fa fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li>
                    <a href="/warehouses">
                        <i class="fa fa-home"></i>
                        <p>Warehouses</p>
                    </a>
                </li>
                <li>
                    <a href="/destinations">
                        <i class="fa fa-map-marker"></i>
                        <p>Destinations</p>
                    </a>
                </li>
                <li>
                    <a href="/transactions">
                        <i class="fa fa-exchange"></i>
                        <p>Transactions</p>
                    </a>
                </li>
                <li class="active">
                    <a href="/costs">
                        <i class="fa fa-usd"></i>
                        <p>Costs</p>
                    </a>
                </li>
				<li>
                    <a href="/company">
                        <i class="fa fa-building"></i>
                        <p>MyCompany</p>
                    </a>
                </li>
				<li>
                    <a href="/logs">
                        <i class="fa fa-history"></i>
                        <p>Logs</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Costs</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                            </div>
                            <div class="content table-responsive table-full-width">
                            <table class="table table-striped table-bordered" style="width:100%">
                                <tbody class="nwcReady">
                                
                                </tbody>
                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

        <hr>
    </div>
</div>

<script>
    ////////////////////////////////////// functions //////////////////////////////////////////////////////////////
    function getAll() {
        $.get("http://localhost:8000/api/nwc/", function(data, status) {
            console.log(data, status)
            if (status) {
<<<<<<< HEAD
                let no = 0;
                let array = [] 
                data.forEach(item, index => {
                    array.push(
                        []
                    )
                    // var t = $('#nwcReady')
                    // t.append("<tr><td>"+element.warehouse_name+"</td></tr>")
=======
                var t = $('.nwcReady')
                data.cost.forEach(element => {
                  length = element.length
                  var tds = element.map(function(item) {
                    console.log(item)
                    return "<td>"+item.cost+"</td>";
                  });
                  t.append('<tr>' + tds.join('') + '</tr>');
>>>>>>> c9384d544771f905784588941d34f348efcd9c99
                });
                var demands = []
                for (i=0; i < length; i++) {
                    demands.push("<td><input type='text' id='demand["+i+"]' name='demand[]'></td>");
                }
                t.append('<tr>' + demands + '</tr>');
            } else {
                alert('Gagal Load Data!')
            }
        });
    }

    function clearTable() {
        var table = $('#example').DataTable();
        table
            .clear()
            .draw();
    }

    $(document).ready(function() {
        getAll();

      // $(document).ready(function() {
      //     $('#example').DataTable();
      // } );
    })
</script>
@endsection