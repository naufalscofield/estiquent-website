@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">
  <!--
    Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
    Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->
  <div class="sidebar-wrapper">
    <div class="logo">
      <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
      </a>
    </div>
    <ul class="nav">
      <li>
        <a href="/admin">
          <i class="fa fa-tachometer"></i>
          <p>Dashboard</p>
        </a>
      </li>
      <li>
        <a href="/profile">
          <i class="fa fa-user"></i>
          <p>Profile</p>
        </a>
      </li>
      <li>
        <a href="/users">
          <i class="fa fa-users"></i>
          <p>Users</p>
        </a>
      </li>
      <li>
        <a href="/warehouses">
          <i class="fa fa-home"></i>
          <p>Warehouses</p>
        </a>
      </li>
      <li>
        <a href="/destinations">
          <i class="fa fa-map-marker"></i>
          <p>Destinations</p>
        </a>
      </li>
      <li>
        <a href="/transactions">
          <i class="fa fa-exchange"></i>
          <p>Transactions</p>
        </a>
      </li>
      <li class="active">
        <a href="/costs">
          <i class="fa fa-usd"></i>
          <p>Costs</p>
        </a>
      </li>
      <li>
        <a href="/company">
          <i class="fa fa-building"></i>
          <p>MyCompany</p>
        </a>
      </li>
      <li>
        <a href="/logs">
          <i class="fa fa-history"></i>
          <p>Logs</p>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="main-panel">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar bar1"></span>
        <span class="icon-bar bar2"></span>
        <span class="icon-bar bar3"></span>
        </button>
        <a class="navbar-brand">Costs</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="/logout">
              <i class="fa fa-sign-out"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="header">
            </div>
            <div class="content table-responsive table-full-width">
              <input type='hidden' id='destinationLength' />
              <table class="table table-striped table-bordered" style="width:100%">
                <tbody class="nwcReady">
                </tbody>
              </table>
              <div style="text-align: center">
                <button id="calculate" class="btn btn-success btn-fill">Calculate!</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<hr>
</div>
</div>

<script>
  ////////////////////////////////////// functions //////////////////////////////////////////////////////////////
  function getAll() {
    $.get("http://localhost:8000/api/nwc/", function (data, status) {
      // console.log(data, status)
      if (status) {


        var length
        var t = $('.nwcReady')
        data.cost.forEach((element, idxCost) => {
          length = element.length
          $('#destinationLength').val(length)
          
          let tds = []
          element.forEach((item, index) => {
            console.log(item)
            tds.push("<td id='hasil-"+idxCost+"-"+index+"'></td><td width='25px' style='background-color: #F27625;'>" + item.cost + "</td>");
          });
          tds.push('<td>' + data.cpy[idxCost].capacity + '</td>');
          t.append('<tr>' + tds.join('') + '</tr>');
        });
        var demands = []
        for (i = 0; i < length; i++) {
          demands.push("<td colspan=2><input type='text' id='demand" + i + "' class='form-control2' name='demand[]' /></td>");
        }
        t.append('<tr>' + demands + '</tr>');


      } else {
        alert('Gagal Load Data!')
      }
    });
  }

  function clearTable() {
    var table = $('#example').DataTable();
    table
      .clear()
      .draw();
  }

  $(document).ready(function () {
    getAll();
  })

  $(document).on('click', '#calculate', function() {
    let panjang = $('#destinationLength').val()
    let arrDemand = []
    for (let i=0; i < panjang; i++) {
      arrDemand.push(parseInt($('#demand'+i).val()))
    }

    console.log(arrDemand)
    let hasil = []
    $.get("http://localhost:8000/api/nwc/", function (data, status) {
      console.log('data.length',data)
      let num = 0
      for (let i=0; i < data.cost.length; i++) {
        let tempHasil = []
        let val = arrDemand[i]
        console.log('data[i]',data.cost[i])
        for (let j=0; j < data.cost[i].length; j++) {
          if(arrDemand[num] == 0 && data.cpy[i].capacity == 0) {
            // jika demand sudah habis, maka pindah demand selanjutnya
            num = num+1
          }
          console.log('hehe koma',arrDemand[num], data.cpy[i].capacity)
          if (arrDemand[num] > data.cpy[i].capacity) {
            // jika demand lebih besar daripada capacity
            if (data.cpy[i].capacity == 0) {
              // jika capacity sudah 0, maka selanjutnya tetap akan 0
              tempHasil.push(0)
              console.log('cara 1a : sisa', arrDemand[num], data.cpy[i].capacity)
            }
            else {
              // jia capacity > 0, maka demand dikurang capacity dan capacity menjadi 0
              console.log('cara 1b : sisa', arrDemand[num], data.cpy[i].capacity)
              arrDemand[num] = arrDemand[num] - data.cpy[i].capacity
              tempHasil.push(data.cpy[i].capacity)
              data.cpy[i].capacity = data.cpy[i].capacity - data.cpy[i].capacity
              console.log('demand after', arrDemand[num])
              console.log('capacity after', data.cpy[i].capacity)
            }
          }
          else if (arrDemand[num] < data.cpy[i].capacity) {
            // jika demand < capacity, maka masukan semua demand dan capacity kurang demand
            console.log('cara 2 : sisa', arrDemand[num], data.cpy[i].capacity)
            tempHasil.push(arrDemand[num])
            arrDemand[num] = arrDemand[num] - arrDemand[num]
            data.cpy[i].capacity = data.cpy[i].capacity - arrDemand[num]
            console.log('demand after', arrDemand[num])
            console.log('capacity after', data.cpy[i].capacity)
          }
          else if (arrDemand[num] == data.cpy[i].capacity) {
            // jika demand == capacity, maka habis, dam masukan sisanya
            console.log('cara 3 : sisa', arrDemand[num], data.cpy[i].capacity)
            tempHasil.push(arrDemand[num])
            data.cpy[i].capacity = data.cpy[i].capacity - arrDemand[num]
            console.log('demand after', arrDemand[num])
            console.log('capacity after', data.cpy[i].capacity)
          }

          // tempArray.push()
          console.log('num',num)
          console.log('tempHasil',tempHasil)
          console.log('cpy',data.cpy[i].capacity)
          console.log('data[i][j]',data.cost[i][j])  
          console.log('---------------------------------------------')      
        }     
        hasil.push(tempHasil)   
        console.log('hasil',hasil)
      }
      hasil.forEach((itemWarehouse, idxWarehouse) => {
        hasil[idxWarehouse].forEach((itemDestination, idxDestination) => {
          $('#hasil-'+idxWarehouse+'-'+idxDestination).html(itemDestination)
        })
      })
    })
    

    // console.log(arrDemand)
  });
  
</script>
@endsection