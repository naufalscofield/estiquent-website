@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<style>
        #myInput {
            /* background-image: url('/css/filter.png'); Add a search icon to input */
            background-position: 10px 12px; /* Position the search icon */
            background-repeat: no-repeat; /* Do not repeat the icon image */
            width: 30%; /* Full-width */
            color: grey;
            font-size: 16px; /* Increase font-size */
            padding: 1px 1px 12px 1px; /* Add some padding */
            border: 2px solid #9999ff; /* Add a grey border */
            margin-bottom: 12px; /* Add some space below the input */
        }
 </style>       
 <!-- CSS for DataTable     -->
 <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
 <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />
 <div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="/admin">
                    <i class="fa fa-tachometer"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="/profile">
                    <i class="fa fa-user"></i>
                    <p>Profile</p>
                </a>
            </li>
            <li>
                <a href="/users">
                    <i class="fa fa-users"></i>
                    <p>Users</p>
                </a>
            </li>
            <li>
                <a href="/warehouses">
                    <i class="fa fa-home"></i>
                    <p>Warehouses</p>
                </a>
            </li>
            <li>
                <a href="/destinations">
                    <i class="fa fa-map-marker"></i>
                    <p>Destinations</p>
                </a>
            </li>
            <li class="active">
                <a href="/transactions">
                    <i class="fa fa-exchange"></i>
                    <p>Transactions</p>
                </a>
            </li>
            <li>
                <a href="/costs">
                    <i class="fa fa-usd"></i>
                    <p>Costs</p>
                </a>
            </li>
            <li>
                <a href="/company">
                    <i class="fa fa-building"></i>
                    <p>MyCompany</p>
                </a>
            </li>
            <li>
                <a href="/logs">
                    <i class="fa fa-history"></i>
                    <p>Logs</p>
                </a>
            </li>
            <li>
                <a href="/nwc">
                    <i class="fa fa-sort-numeric-asc"></i>
                    <p>nwc</p>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Transactions</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                    <center>
                                        <label for=""><i>Filter Status</i></label>
                                            <input type="hidden" id="company_code" value="{{ $company_code }}">
                                                <select id="myInput" onkeyup="filter()">
                                                    <option selected value="">All</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="approved">Approved</option>
                                                    <option value="rejected">Rejected</option>
                                                </select>
                                    </center>    
                                <h4 class="title"></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <input type="hidden" id="company_code" value="{{ $company_code }}">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <th><center><b>No</b></center></th>
                                        <th><center><b>Transaction Code</b></center></th>
                                    	<th><center><b>Warehouses</b></center></th>
                                    	<th><center><b>Destinations</b></center></th>
                                    	<th><center><b>Total Cost</b></center></th>
                                    	<th><center><b>Total Demand</b></center></th>
                                    	<th><center><b>Date</b></center></th>
                                    	<th><center><b>Status</b></center></th>
                                    	<th><center><b>Actions</b></center></th>
                                    </thead>
                                    <tbody>
                                        <!-- -->
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <script>
            function setNumber(number) {
            return number.toString().replace(/\.00$/,'').replace(/\B(?=(\d{3})+(?!\d))/g, "."); 
            }

            function getAll() {
            var company_code = $("#company_code").val();
            $.get("http://localhost:8000/api/transactions/" + company_code, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.transactions.forEach((element, idx) => {
                        var t = $('#example').DataTable();
                        let warehouse_name = data.warehouses[idx].map(x => x.name).join()
                        let destination_name = data.destinations[idx].map(x => x.name).join()
                        no++;
                        t.row.add( [
                            "<center>"+no+"</center>",
                            "<center>"+element.transaction_code+"</center>",
                            "<center>"+warehouse_name+"</center>",
                            "<center>"+destination_name+"</center>",
                            "<center>Rp."+setNumber(element.total_cost)+"</center>",
                            "<center>"+element.total_demand+"</center>",
                            "<center>"+element.date+"</center>",
                            "<center>"+element.status+"</center>",
                            "<center><a id='btn_delete' data-id="+element.id+" class='btn btn-danger'><i class='fa fa-trash'></i></a><a id='btn_approve' data-id="+element.id+" class='btn btn-success'><i class='fa fa-check'></i></a><a id='btn_reject' data-id="+element.id+" class='btn btn-warning'><i class='fa fa-times'></i></a></center>"
                        ] ).draw( false );
                    });
                }
                else {
                    alert('Gagal Load Data!')
                }
            });
        }
        
        function filter() {
        // Declare variables 
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("example");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[7];
            if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
            } 
        }
        }

        function clearTable() {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
        }

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getAll()
            
            $(document).on('click', '#btn_delete' ,function(){
                // console.log('tes')
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost:8000/api/delete-transaction/' + id,
                type: 'DELETE',
                success: function(data) {
                    clearTable()
                    getAll();
                    toastr.info('Delete Data Success!')
                    }
                });
            });
            
            $(document).on('click', '#btn_approve' ,function(){
                var id               = $(this).data("id");
                var status           = 'approved';
            
                var data = {
                    status : status,
                    }

                jQuery.ajax({
                url: 'http://localhost:8000/api/update-transaction/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()
                    getAll();
                    toastr.success('Update Data Success!')
                    }
                });

            });
            
            $(document).on('click', '#btn_reject' ,function(){
                var id               = $(this).data("id");
                var status           = 'rejected';
            
                var data = {
                    status : status,
                    }

                jQuery.ajax({
                url: 'http://localhost:8000/api/update-transaction/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()
                    getAll();
                    toastr.success('Update Data Success!')
                    }
                });

            });

        } );        
        </script>
@endsection