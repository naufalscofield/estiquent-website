@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="/admin">
                        <i class="fa fa-tachometer"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/profile">
                        <i class="fa fa-user"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li>
                    <a href="/users">
                        <i class="fa fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li>
                    <a href="/warehouses">
                        <i class="fa fa-home"></i>
                        <p>Warehouses</p>
                    </a>
                </li>
                <li>
                    <a href="/destinations">
                        <i class="fa fa-map-marker"></i>
                        <p>Destinations</p>
                    </a>
                </li>
                <li>
                    <a href="/transactions">
                        <i class="fa fa-exchange"></i>
                        <p>Transactions</p>
                    </a>
                </li>
                <li>
                    <a href="/costs">
                        <i class="fa fa-usd"></i>
                        <p>Costs</p>
                    </a>
                </li>
				<li class="active">
                    <a href="/company">
                        <i class="fa fa-building"></i>
                        <p>MyCompany</p>
                    </a>
                </li>
				<li>
                    <a href="/logs">
                        <i class="fa fa-history"></i>
                        <p>Logs</p>
                    </a>
                </li>
                <li>
                    <a href="/nwc">
                        <i class="fa fa-sort-numeric-asc"></i>
                        <p>nwc</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">My Company</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/logout">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="card card-user">
                        <div class="image">
                            <img src="/css/admin/img/background.jpg" alt="..."/>
                        </div>
                        <div class="content">
                            <div class="author">
                              <img class="avatar border-white" src="/css/admin/img/faces/face-2.jpg" alt="..."/>
                              <h4 class="title">PT.Sukarasa Berjaya Sukses<br/>
                                <p class="description text-center">Jln.Sukarasa Bandung 57</p>
                              </h4>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Company Profile</h4>
                        </div>
                        <div class="content">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <input type="text" required class="form-control border-input" placeholder="Company Name" value="" name="company_name">
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Company Code</label>
                                            <input type="text" required class="form-control border-input" disabled placeholder="Company Code" value="" name="company_code">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Logo</label>
                                            <input type="file" required class="form-control border-input" name="logo">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea name="description" required class="form-control border-input" placeholder="Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea name="address" required class="form-control border-input" placeholder="address"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-info btn-fill btn-wd">Update Company Profile</button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection