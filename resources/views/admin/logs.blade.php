@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
 <!-- CSS for DataTable     -->
 <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
 <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />
<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Logs</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <th><center><b>No</b></center></th>
                                    	<th><center><b>User</b></center></th>
                                    	<th><center><b>Activity</b></center></th>
                                    	<th><center><b>Date</b></center></th>
                                    	<th><center><b>Actions</b></center></th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        	<td><center>1</center></td>
                                        	<td><center>Dakota Rice</center></td>
                                        	<td><center>$36,738</center></td>
                                        	<td><center>Niger</center></td>
                                            <td><center><a href="" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            </center></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );        
        </script>
@endsection