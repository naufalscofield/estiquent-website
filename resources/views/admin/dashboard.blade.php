<script type="text/javascript" src="{{ asset('js/fusioncharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fusioncharts.charts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fusioncharts.theme.fint.js') }}"></script>
<link href="/css/admin/themify-icons.css" rel="stylesheet">
@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
            </a>
        </div>

        <ul class="nav">
            <li class="active">
                <a href="/admin">
                    <i class="fa fa-tachometer"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="/profile">
                    <i class="fa fa-user"></i>
                    <p>Profile</p>
                </a>
            </li>
            <li>
                <a href="/users">
                    <i class="fa fa-users"></i>
                    <p>Users</p>
                </a>
            </li>
            <li>
                <a href="/warehouses">
                    <i class="fa fa-home"></i>
                    <p>Warehouses</p>
                </a>
            </li>
            <li>
                <a href="/destinations">
                    <i class="fa fa-map-marker"></i>
                    <p>Destinations</p>
                </a>
            </li>
            <li>
                <a href="/transactions">
                    <i class="fa fa-exchange"></i>
                    <p>Transactions</p>
                </a>
            </li>
            <li>
                <a href="/costs">
                    <i class="fa fa-usd"></i>
                    <p>Costs</p>
                </a>
            </li>
            <li>
                <a href="/company">
                    <i class="fa fa-building"></i>
                    <p>MyCompany</p>
                </a>
            </li>
            <li>
                <a href="/logs">
                    <i class="fa fa-history"></i>
                    <p>Logs</p>
                </a>
            </li>
            <li>
                <a href="/nwc">
                    <i class="fa fa-sort-numeric-asc"></i>
                    <p>nwc</p>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">Dashboard</a>
                {{-- <input type="hidden" id="company_code" value="{{ $company_code }}"> --}}
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/logout">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>


    <div class="content">
        <div class="container-fluid">
            <div class="row">
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Transactions Graph by Month</h4><br>
                            <div class="col-md-3">
                            <p class="category">
                                <form method="get" action="/admin">
                                    <input type="text" name="year" id="year" placeholder="Year" class="form-control border-input">
                                </div>
                                    <input type="submit" name="submit" class="btn btn-success btn-fill btn-s" value="Get" ></p>
                                </form>
                        </div>
                        <div>
                            <?php 
                                if(isset($_GET['year'])) { ?>
                                <div id="graph" style="width: 100%; height: 400px;"></div>
                                <center><table id="datatable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="color:#0e4379;font-size:25px;text-align:center">Month</th>
                                            <th style="color:#0e4379;font-size:25px;text-align:center">Amount</th>
                                            {{-- @forEach($graph as $datas)
                                            
                    
                                                <td>{{ $datas->bln }}-{{ $datas->jml }} Transactions</td>

                                            @endforeach     --}}
    
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @forEach($graph as $datas)
                                            <tr>
                                                <th style="color:#f27624;font-size:20px;text-align:center">{{ $datas->bln }}</th>
                                                <td style="color:#f27624;font-size:20px;text-align:center">{{ $datas->jml }}</td>
                                            </tr>
                                            @endforeach  
                                        </tbody>
                                    </table>
                               <?php }else{ ?>
                                <div id="graph" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
                                <table id="datatable">
                                <thead>
                                    <tr>
                                        

                                    </tr>
                                </thead>
                                <tbody>
                                        
                                    </tbody>
                                </table>
                            <?php  } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            Highcharts.chart('graph', {
                data: {
                    table: 'datatable'
                },
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Transactions Graph by Month'
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Units'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            this.point.y + ' ' + this.point.name.toLowerCase();
                    }
                }
            });
        })
    </script>
@endsection