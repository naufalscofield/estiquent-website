@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">
  <!--
    Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
    Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->
  <div class="sidebar-wrapper">
    <div class="logo">
      <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
      </a>
    </div>
    <ul class="nav">
      <li>
        <a href="/admin">
          <i class="fa fa-tachometer"></i>
          <p>Dashboard</p>
        </a>
      </li>
      <li>
        <a href="/profile">
          <i class="fa fa-user"></i>
          <p>Profile</p>
        </a>
      </li>
      <li>
        <a href="/users">
          <i class="fa fa-users"></i>
          <p>Users</p>
        </a>
      </li>
      <li>
        <a href="/warehouses">
          <i class="fa fa-home"></i>
          <p>Warehouses</p>
        </a>
      </li>
      <li>
        <a href="/destinations">
          <i class="fa fa-map-marker"></i>
          <p>Destinations</p>
        </a>
      </li>
      <li>
        <a href="/transactions">
          <i class="fa fa-exchange"></i>
          <p>Transactions</p>
        </a>
      </li>
      <li class="active">
        <a href="/costs">
          <i class="fa fa-usd"></i>
          <p>Costs</p>
        </a>
      </li>
      <li>
        <a href="/company">
          <i class="fa fa-building"></i>
          <p>MyCompany</p>
        </a>
      </li>
      <li>
        <a href="/logs">
          <i class="fa fa-history"></i>
          <p>Logs</p>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="main-panel">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar bar1"></span>
        <span class="icon-bar bar2"></span>
        <span class="icon-bar bar3"></span>
        </button>
        <a class="navbar-brand">Costs</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="/logout">
              <i class="fa fa-sign-out"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="header">
            </div>
            <form action="javascript::" onsubmit="calculate()">
              <input type="hidden" id="company_code" value="{{ $company_code }}">
              <div class="content table-responsive table-full-width">
                <input type='hidden' id='destinationLength' />
                <table class="table table-striped table-bordered" style="width:100%">
                  <tbody class="nwcReady">
                  </tbody>
                </table>
                <div style="text-align: center">
                  <button type="submit" class="btn btn-success btn-fill">Calculate!</button>
                </div>
                <div style="text-align: center; margin: 30px">
                  <span>
                    Hasil Hitung Harga
                  <span>
                  <br />
                  <br />
                  <div id="cost-results">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
<hr>
</div>
</div>

<script>
  let cost_global
  let total_demand = 0
  let dataNWC
  let totalPrice = 0
  ////////////////////////////////////// functions //////////////////////////////////////////////////////////////
  function getAll() {
    $.get("http://localhost:8000/api/nwc/", function (data, status) {
      dataNWC = data
      // console.log(data, status)
      if (status) {

        cost_global = data.cost
        data_nwc = data

        console.log('data_nwc', data_nwc)

        var length
        var t = $('.nwcReady')
        data.cost.forEach((element, idxCost) => {
          total_demand += data.cpy[idxCost].capacity
          length = element.length
          $('#destinationLength').val(length)
          
          let tds = []
          let thead = []
          tds.push("<td width='150px' style='text-align: center; font-weight: 650'>" + data.warehouse[idxCost].name + "</td>");
          thead.push("<td style='background-color: #bdc3c7'></td>");
          element.forEach((item, index) => {
            console.log(item)
            thead.push("<td colspan='2' style='text-align: center; font-weight: 650'>"+data.destination[index].name+"</td>");
            tds.push("<td id='hasil-"+idxCost+"-"+index+"' style='text-align: center;'></td><td width='25px' style='background-color: #F27625; text-align: center;'>" + item.cost + "</td>");
          });
          thead.push("<td width='100px' style='text-align: center; font-weight: 650'>Cpy</td>");
          if (idxCost == 0) {
            t.append('<tr>' + thead.join('') + '</tr>');  
          }
          tds.push('<td>' + data.cpy[idxCost].capacity + '</td>');
          t.append('<tr>' + tds.join('') + '</tr>');
        });
        var demands = []
        demands.push("<td></td>");
        for (i = 0; i < length; i++) {
          demands.push("<td colspan=2><input type='number' min='1' id='demand" + i + "' class='form-control2' name='demand[]' /></td>");
        }
        demands.push("<td></td>");
        t.append('<tr>' + demands + '</tr>');


      } else {
        alert('Gagal Load Data!')
      }
    });
  }

  function clearTable() {
    var table = $('#example').DataTable();
    table
      .clear()
      .draw();
  }

  $(document).ready(function () {
    getAll();
  })

  function calculate() {
    let panjang = $('#destinationLength').val()
    let arrDemand = []
    for (let i=0; i < panjang; i++) {
      arrDemand.push(parseInt($('#demand'+i).val()))
    }

    console.log('arrDemand',arrDemand)
    let hasil = []
    var company_code = $('#company_code').val()
    $.get("http://localhost:8000/api/nwc/" + company_code, function (data, status) {
      console.log('data.length',data)
      let stepke = 1
      let num = 0
      let skip = 0
      for (let i=0; i < data.cost.length; i++) {
        let tempHasil = []
        let val = arrDemand[i]
        let demand0 = arrDemand[0]
        console.log('data[i]',data.cost[i])
        if (skip > 0) {
          for(let k=0; k < skip; k++) {
            console.log('skippp')
            tempHasil.push(0)
          }
        }
        console.log('DEMAND NEW ROW', arrDemand[num])
        // if(arrDemand[num] == 0) {
        //     // jika demand sudah habis, maka pindah demand selanjutnya
        //     num = num+1
        //   }
        for (let j=0; j < data.cost[i].length; j++) {
          let isSkipped = false
          console.log('step ke', stepke++)
          console.log('hehe koma',arrDemand[num], data.cpy[i].capacity)
          if (arrDemand[num] > data.cpy[i].capacity) {
            // jika demand lebih besar daripada capacity
            if (data.cpy[i].capacity == 0) {
              // jika capacity sudah 0, maka selanjutnya tetap akan 0
              tempHasil.push(0)
              console.log('cara 1a : sisa', arrDemand[num], data.cpy[i].capacity)
              // skip = skip+1
            }
            else {
              // jia capacity > 0, maka demand dikurang capacity dan capacity menjadi 0
              console.log('cara 1b : sisa', arrDemand[num], data.cpy[i].capacity)
              arrDemand[num] = arrDemand[num] - data.cpy[i].capacity
              tempHasil.push(data.cpy[i].capacity)
              data.cpy[i].capacity = data.cpy[i].capacity - data.cpy[i].capacity
              console.log('demand after', arrDemand[num])
              console.log('capacity after', data.cpy[i].capacity)
            }
          }
          else if (arrDemand[num] < data.cpy[i].capacity) {
            // jika demand < capacity, maka masukan semua demand dan capacity kurang demand
            if (arrDemand[num] == 0 && data.cpy[i].capacity != 0) {
              console.log('cara 2a : sisa', arrDemand[num], data.cpy[i].capacity)
              tempHasil.push(0)
              num = num+1
              if (demand0 == 0) {
                console.log('tambah skip')
                skip = skip+1
                isSkipped = true
              }
              // tempHasil.push(data.cpy[i].capacity)
              // data.cpy[i].capacity = data.cpy[i].capacity - data.cpy[i].capacity
            }
            else {
              console.log('cara 2b : sisa', arrDemand[num], data.cpy[i].capacity)
              tempHasil.push(arrDemand[num])
              data.cpy[i].capacity = data.cpy[i].capacity - arrDemand[num]
              arrDemand[num] = arrDemand[num] - arrDemand[num]
              if (arrDemand[num] == 0 && data.cpy[i].capacity != 0){
                console.log('tambah skip')
                skip = skip+1
                isSkipped = true
              }
              num = num+1
            }
            console.log('demand after', arrDemand[num])
            console.log('capacity after', data.cpy[i].capacity)
          }
          else if (arrDemand[num] == data.cpy[i].capacity) {
            // if (arrDemand[num] == 0 && data.cpy[i].capacity == 0) {
            //   num = num+1
            // }
            // jika demand == capacity, maka habis, dam masukan sisanya
            console.log('cara 3 : sisa', arrDemand[num], data.cpy[i].capacity)
            tempHasil.push(arrDemand[num])
            data.cpy[i].capacity = data.cpy[i].capacity - arrDemand[num]
            arrDemand[num] = arrDemand[num] - arrDemand[num]
            console.log('demand after', arrDemand[num])
            console.log('capacity after', data.cpy[i].capacity)
            console.log('tambah skip')
            skip = skip+1
            isSkipped = true
          }

          // tempArray.push()
          console.log('num',num)
          console.log('tempHasil',tempHasil)
          console.log('cpy',data.cpy[i].capacity)
          console.log('data[i][j]',data.cost[i][j])  
          if(arrDemand[num] == 0 && data.cpy[i].capacity == 0) {
            // jika demand sudah habis, maka pindah demand selanjutnya
            console.log('tambah skip')
            num = num+1
          }
          else if (arrDemand[num] == 0 && data.cpy[i].capacity != 0){
            if (!isSkipped) {
              console.log('tambah skip')
              skip = skip+1
            }

          }
          // if (arrDemand[num-1] == 0) {
          //   console.log('tambah skip')
          //   skip = skip+1
          // }

          console.log('---------------------------------------------')      
        }     
        hasil.push(tempHasil)   
        console.log('hasil',hasil)
      }
      hasil.forEach((itemWarehouse, idxWarehouse) => {
        hasil[idxWarehouse].forEach((itemDestination, idxDestination) => {
          $('#hasil-'+idxWarehouse+'-'+idxDestination).html(itemDestination)
        })
      })

      var result = $('#cost-results')
      let cr = []
      for (let sup=0; sup < hasil.length; sup++) {
        for (let dem=0; dem < hasil[0].length; dem++) {
          result.append("<p>Supply "+(sup+1)+" x Demand "+(dem+1)+" = "+hasil[sup][dem]+" x "+cost_global[sup][dem].cost+" = "+hasil[sup][dem] * cost_global[sup][dem].cost+" </p>");      
          totalPrice += (hasil[sup][dem] * cost_global[sup][dem].cost)
        }
      }

      result.append("<hr /> <p>TOTAL COST = Rp "+ totalPrice);
      result.append("<button type='button' class='btn btn-primary' id='save-transaction'>SIMPAN</button>")      
      // result.append(cr);
    })

    $(document).on('click', '#save-transaction', function(){
      alert(total_demand+" - "+totalPrice)
      const id_destination = dataNWC.destination.map(x => x.id).join();
      const id_warehouse = dataNWC.warehouse.map(x => x.id).join();
      var company_code = $('#company_code').val() 
      console.log(id_destination)
      let data = {
        total_demand,
        id_warehouse,
        id_destination,
        company_code: company_code,
        total_costs: totalPrice
      }

      $.ajax({
        url: 'http://localhost:8000/api/insert-transaction/',
        type: 'POST',
        data : data,
        success: function(data) {
          let resp = JSON.parse(data)
          console.log(data)
          if (resp.status) {
            toastr.success('Success!', 'Data berhasil ditambahkan.')
          }
        }
      });
    })
  };

  
</script>
@endsection