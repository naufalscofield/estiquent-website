@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="/admin">
                        <i class="fa fa-tachometer"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/profile">
                        <i class="fa fa-user"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li>
                    <a href="/users">
                        <i class="fa fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li>
                    <a href="/warehouses">
                        <i class="fa fa-home"></i>
                        <p>Warehouses</p>
                    </a>
                </li>
                <li>
                    <a href="/destinations">
                        <i class="fa fa-map-marker"></i>
                        <p>Destinations</p>
                    </a>
                </li>
                <li>
                    <a href="/transactions">
                        <i class="fa fa-exchange"></i>
                        <p>Transactions</p>
                    </a>
                </li>
                <li class="active">
                    <a href="/costs">
                        <i class="fa fa-usd"></i>
                        <p>Costs</p>
                    </a>
                </li>
				<li>
                    <a href="/company">
                        <i class="fa fa-building"></i>
                        <p>MyCompany</p>
                    </a>
                </li>
				<li>
                    <a href="/logs">
                        <i class="fa fa-history"></i>
                        <p>Logs</p>
                    </a>
                </li>
                <li>
                    <a href="/nwc">
                        <i class="fa fa-sort-numeric-asc"></i>
                        <p>nwc</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Costs</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="table table-striped">
                                        <input type="hidden" id="company_code" value="{{ $company_code }}">
                                    <thead>
                                        <th width="10"><center><b>No</b></center></th>
                                        <th width="20"><center><b>Warehouse</b></center></th>
                                        <th width="20"><center><b>Destination</b></center></th>
                                        <th width="20"><center><b>Type</b></center></th>
                                        <th width="20"><center><b>Moda</b></center></th>
                                        <th width="20"><center><b>Cost</b></center></th>
                                        <th width="20"><center><b>Actions</b></center></th>
                                    </thead>
                                    <tbody id="table-row">
                                           <!-- -->
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="editCost" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Cost</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">
                                    <!-- -->    
                                    </div>
                                </div>
                                {{-- <div class="col-md-12">
                                    <div class="form-group" id="input_id_warehouse">
                                    <!-- -->    
                                    </div>
                                </div> --}}
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_warehouse_name">
                                    <!-- --> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input_destination_name">
                                    <!-- -->   
                                    </div>
                                </div>
                            </div>
        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_type">
                                     <!-- --> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input_moda">
                                    <!-- -->   
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_cost">
                                    <!-- -->   
                                    </div>
                                </div>
                            </div>
        
                            <div class="text-center">
                                {{ csrf_field() }}
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Cost
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <hr>
    </div>
</div>
    <script>

////////////////////////////////////// functions //////////////////////////////////////////////////////////////

        function getAll() {
            var company_code = $("#company_code").val();
            $.get("http://localhost:8000/api/costs/" + company_code, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
            var t = $('#example').DataTable();
            no++;
            t.row.add( [
                "<center>"+no+"</center>",
                "<center>"+element.warehouse_name+"</center>",
                "<center>"+element.destination_name+"</center>",
                "<center>"+element.type+"</center>",
                "<center>"+element.moda+"</center>",
                "<center>"+element.cost+"</center>",
                "<center><a id='btn_delete' data-id="+element.id+" class='btn btn-danger'><i class='fa fa-trash'></i></a><a id='btn_edit' data-idedit="+element.id+" class='btn btn-info' data-toggle='modal' data-target='#editCost'><i class='fa fa-pencil'></i></a></center>"
            ] ).draw( false );
                    });
                }
                else {
                    alert('Gagal Load Data!')
                }
            });
        }

        function clearTable() {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
        }

 /////////////////////////////////////// document ready functions ///////////////////////////////////////////////

        $(document).ready(function() {
            $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
                ],
            } );

            
            getAll()


            $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("idedit");
                console.log(id)
                $.get("http://localhost:8000/api/cost/" + id, function(data, status){
                console.log(data, status)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required disabled id='id_edit' class='form-control border-input' placeholder='Id' value="+element.id+">");
                    // $('#input_id_warehouse').html("")
                    //     $('#input_id_warehouse').append("<label>Company</label><input type='text' required disabled id='company_name_edit' class='form-control border-input' placeholder='Company' value="+element.company_name+">");
                    $('#input_warehouse_name').html("")
                        $('#input_warehouse_name').append("<label>Warehouse Name</label><input type='text' disabled required id='warehouse_name_edit' class='form-control border-input' placeholder='warehouse name' value="+element.warehouse_name+">");
                    // $('#input_id_destination').html("")
                    //     $('#input_id_destination').append("<label>Warehouse Name</label><input type='text' disabled required id='name_edit' class='form-control border-input' placeholder='name' value="+element.name+">");
                    $('#input_destination_name').html("")
                        $('#input_destination_name').append("<label>Destination Name</label><input type='text' disabled required id='destination_name_edit' class='form-control border-input' placeholder='destination name' value="+element.destination_name+">");
                    $('#input_type').html("")
                        $('#input_type').append("<label>Type</label><input type='text' disabled required id='type_edit' class='form-control border-input' placeholder='type' value="+element.type+">");
                    $('#input_moda').html("")
                        $('#input_moda').append("<label>Moda</label><select class='form-control border-input' id='moda_edit'><option class='form-control border-input' value='truck'>Truck</option><option class='form-control border-input' value='ship'>Ship</option><option class='form-control border-input' value='plane'>Plane</option></select>");
                    $('#input_cost').html("")
                        $('#input_cost').append("<label>Cost</label><input type='text' onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id='cost_edit' class='form-control border-input' placeholder='cost' value="+element.cost+">");
                    });
                }
                else {
                    toastr.danger('Load Data Failed!')
                    }
                });
            });     

            
            $(document).on('click', '#btn_delete' ,function(){
                // console.log('tes')
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost:8000/api/delete-cost/' + id,
                type: 'DELETE',
                success: function(data) {
                    clearTable()
                    getAll();
                    toastr.info('Delete Data Success!')
                    }
                });
            });
            
            
            $(document).on('submit', '#update' ,function(){
                var id               = $('#id_edit').val();
                var moda             = $('#moda_edit').val();
                var cost             = $('#cost_edit').val();
    
                var data = {
                    moda : moda,
                    cost : cost,
                    }

                jQuery.ajax({
                url: 'http://localhost:8000/api/update-cost/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()
                    $('#editCost').modal('hide')
                    $('#id_edit').val('')
                    $('#warehouse_name_edit').val('')
                    $('#destination_name_edit').val('')
                    $('#type_edit').val('')
                    $('#moda_edit').val('')
                    $('#cost_edit').val('')
                 
                    getAll();
                    toastr.success('Update Data Success!')
                    }
                });

            });
            
        } );        
    </script>
@endsection