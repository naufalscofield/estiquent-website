<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Softdeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Destination extends Authenticatable
{
    use Notifiable;
    use Softdeletes;

    protected $guarded = [];
}
