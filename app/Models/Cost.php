<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Softdeletes;

class Cost extends Model
{
    use Softdeletes;
    
    protected $guarded = []; 
}
