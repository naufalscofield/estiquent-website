<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Transaction;

class Admin extends Controller
{
    public function index()
    {
        // $tahun = $_GET['year'];
        if(isset($_GET['year'])) {
            // $id = session('id_user');
            // $data = User::find($id);
            // $company_code = $data->company_code;
            $year = $_GET['year'];
            $id = session('id_user');
            $data = User::find($id);
            $company_code = $data->company_code;
            $graph = DB::select(
                "SELECT COUNT(id) as jml,monthname(date) as bln FROM transactions where company_code = '".$company_code."' and year(date) = '".$year."' GROUP BY month(date)"
             );
            // $data = $data->graph;
            // print_r($graph); die;
            return view ('admin/dashboard',["graph" => $graph]);
        }else{
            return view ('admin/dashboard');
        }
    }
}
