<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class Users extends Controller
{
    public function index()
    {
        $id = session('id_user');
        // dd($id)
        $data = User::find($id);
        $company_code = $data->company_code;
        // print_r($company_code); die;

        return view ('admin/users',["company_code" => $company_code]);
    }
}
