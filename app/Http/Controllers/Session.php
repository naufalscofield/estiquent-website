<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class Session extends Controller
{
    public function index(Request $request, $id, $pw, $r)
    {
        $request->session()->set('id_user',$id);
        $request->session()->set('password',$pw);
        $request->session()->set('role',$r);
        return redirect('/profile');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }
}
?>