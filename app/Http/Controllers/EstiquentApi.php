<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Company;
use App\Models\Warehouse;
use App\Models\Transaction;
use App\Models\Cost;
use App\Models\Destination;
use App\Models\Log;

class EstiquentApi extends Controller
{
  
  //-------------------------------Crud Profile-------------------------------------------------------------//
  public function getProfile($id)
  {
    // $id = session('id_user');
    // $profile = User::find($id);
    $profile = DB::table('users')
    ->join('companies', 'users.company_code', '=', 'companies.company_code')
    ->where('users.id', '=',$id )
    ->select('first_name','last_name','birth_place','birth_date','username','password','role','users.email as email','photo','gender','company_name')
    ->get();

    if(!$profile)
    abort(404);

    return $profile;
  }

  public function updateProfile(Request $request, $id)
  {
    // update mass assginment
   print_r($request); die;
    User::where('id', $id)->update([
      'first_name'  => $request->first_name,
      'last_name'   => $request->last_name,
      'birth_place' => $request->birth_place,
      'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
      'username'    => $request->username,
      'password'    => md5($request->password),
      'email'       => $request->email,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }
  
  // ----------------------------- Crud User ------------------------------------------------------------- //  
  public function getUser($cc)
  //getdata all
  {
    // $cc = 'est_sukarasa_445';
    $users = DB::table('users')
              ->where([
                ['company_code', $cc],
                ['deleted_at', null],
              ])
              ->get();    
    return $users;
  }

  public function showUser($id)
  {
    //getdata single
    $users = User::find($id);

    if(!$users)
    abort(404);

    return $users;
  }

  public function storeUser(Request $request)
  {
    //insert mass assginment
    User::create([
      'first_name'  => $request->first_name,
      'last_name'   => $request->last_name,
      'birth_place' => $request->birth_place,
      'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
      'username'    => $request->username,
      'password'    => md5($request->password),
      'role'        => $request->role,
      'email'       => $request->email,
      'company_code'=> $request->company_code,
      'photo'       => $request->photo,
      // 'gender'      => $request->gender,
      'gender'      => 'male',
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function updateUser(Request $request, $id)
  {
    // update mass assginment
    User::where('id', $id)->update([
      'first_name'  => $request->first_name,
      'last_name'   => $request->last_name,
      'birth_place' => $request->birth_place,
      'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
      'username'    => $request->username,
      'password'    => $request->password,
      'role'        => $request->role,
      'email'       => $request->email,
      'company_code'=> $request->company_code,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function destroyUser($id)
  {
    // delete dengan fungsi softdelete
    // dd($id);
    User::find($id)->delete();
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function loginUser(Request $request)
  {
    $username = $request->username;
    $password = md5($request->password);

    // print_r($password); die;
    
    $data = User::where('username',$username)->first();
    // print_r($data->password);die;
    if($data)
    { //apakah username tersebut ada atau tidak
      if($password == $data->password)
      {
        return response()->json([
          "status" => true,
          "data" => $data,
        ], 200); // Status code here
      }
      else
      {
        return response()->json([
          "message" => "Login failed,check your username and password!",  
          "status" => false,
        ], 200); // Status code here
      }
    }
      else
      {
        return response()->json([
          "message" => "Login failed,check your username and password!",  
          "status" => false,
        ], 200); // Status code here
      }
      
  }
// -------------------------------- Crud Companies --------------------------------------------------- //
  public function getCompany()
  //getdata all
  {
    $companies = Company::all();
    return $companies;
  }

  public function showCompany($id)
  {
    //getdata single
    $companies = Company::find($id);

    if(!$companies)
    abort(404);

    return $companies;
  }

  public function storeCompany(Request $request, Mailer $mailer)
  {
    $short_name   = $request->short_name;
    $numb         = rand(001,999);
    $company_code = "est_".$short_name."_".$numb;
    //insert mass assginment
    Company::create([
      'company_name'    => $request->company_name,
      'short_name'      => $request->short_name,
      'company_code'    => $company_code,
      'address'         => $request->address,
      'description'     => $request->description,
      'logo'            => '$request->logo',
      'email'           => $request->email,
    ]);
    
    $data = User::create([
      'first_name'      => 'Jane',
      'last_name'       => 'Doe',
      'birth_place'     => 'Earth',
      'birth_date'      => date("Y-m-d"),
      'username'        => $company_code,
      'password'        => md5($company_code),
      'role'            => 'admin',
      'email'           => $request->email,
      'company_code'    => $company_code,
      'photo'           => 'default.jpg',
      'gender'          => 'undefined',
    ]);
      
    // $title = "Hai,thank u for joining Estiquent,your Company Code of your company is".$company_code.".Use that Company Code for login to 
    //             your dashboard as Superadmin of your company and just start it!";

    // $mailer
    //   ->to($request->email)
    //   ->send(new \App\Mail\MyMail($title));      

    $myArr = array(
      "status" => true,
      "kodok" => 'gopul',
      "cc" => $data->company_code
    );
    return $myArr;
  }

  public function updateCompany(Request $request, $id)
  {
    // update mass assginment
    Company::where('id', $id)->update([
      'company_name'  => $request->company_name,
      'company_code'  => $request->company_code,
      'description'   => $request->description,
      'logo'          => $request->logo,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function destroyCompany($id)
  {
    // delete dengan fungsi softdelete
    Company::find($id)->delete();
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }
//----------------------------------- Crud Warehouses -------------------------------------------------//
  public function getWarehouse($cc)
  //getdata all
  {
    $warehouses = DB::table('warehouses')
              ->where([
                ['company_code', $cc],
                ['deleted_at', null],
                ])
              ->get();    
    return $warehouses;
  }

  public function showWarehouse($id)
  {
    //getdata single
    // $warehouses = Warehouse::find($id);
    $warehouses = DB::table('warehouses')
    ->join('companies', 'warehouses.company_code', '=', 'companies.company_code')
    ->where('warehouses.id', '=',$id )
    ->select('code','location','name','capacity','company_name','warehouses.id as id')
    ->get();

    if(!$warehouses)
    abort(404);

    return $warehouses;
  }


  public function selectForNWC($cc)
  {
    $warehouse = DB::table('warehouses')
      ->select('*')
      ->where('company_code', $cc)
      ->where('deleted_at', NULL)
      ->where('status', 'active')
      ->get()
      ->toArray();
    $destination = DB::table('destinations')
      ->select('*')
      ->where('company_code', $cc)
      ->where('deleted_at', NULL)
      ->where('status', 'active')
      ->get()
      ->toArray();

    $newArr = [];
    for ($i=0; $i < count($warehouse); $i++) {
      $newArr2 = [];
      for ($j=0; $j < count($destination); $j++) {
        $newJ = $j+1;
        $nwc = DB::select(
          "SELECT cost as 'cost' FROM costs WHERE id_warehouse =".$warehouse[$i]->id." AND id_destination = ".$destination[$j]->id.""
        );
        array_push($newArr2, ...$nwc);
      }
      array_push($newArr, $newArr2);
    }

    $capacity = DB::table('warehouses')
    ->select('capacity')
    ->where('company_code', $cc)
    ->get();


    return array(
      "cost" => $newArr,
      "cpy" => $capacity,
      "destination" => $destination,
      "warehouse" => $warehouse
    );
  }

  public function storeWarehouse(Request $request)
  {
    //insert mass assginment
    $data = Warehouse::create([
      'code'        => $request->code,
      'name'        => $request->name,
      'location'    => $request->location,
      'capacity'    => $request->capacity,
      'company_code'=> $request->company_code,
    ]);

    $id = $data->id;
    $cc = $request->company_code;

    $destinations = DB::table('destinations')
    ->where([
      ['company_code', $cc],
      ['deleted_at', null],
      ])
    ->get();    
    
    $count = count($destinations);
    if ($count > 0){
      foreach ($destinations as $key) {
        // $idd = $key->id;
        Cost::create([
          'id_warehouse'          => $id,
          'id_destination'        => $key->id,
          'type'                  => 'normal',
          'moda'                  => 'truck',
          'cost'                  => 30,
          'company_code'          => $request->company_code,
          ]);
        }
      }
      
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
    // dd($id);
    
  }

  public function updateWarehouse(Request $request, $id)
  {
    // update mass assginment
    Warehouse::where('id', $id)->update([
      'code'         => $request->all()['code'],
      'name'         => $request->all()['name'],
      'location'     => $request->all()['location'],
      'capacity'     => $request->all()['capacity'],
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }
  public function updateStatusWarehouse(Request $request, $id)
  {
    // update mass assginment
    Warehouse::where('id', $id)->update([
      'status'    => $request->status,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function destroyWarehouse($id)
  {
    // delete dengan fungsi softdelete
    Warehouse::find($id)->delete();
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }
//------------------------------------ Crud Destinations --------------------------------------------//
  public function getDestination($cc)
  //getdata all
  {
    $destinations = DB::table('destinations')
    ->where([
      ['company_code', $cc],
      ['deleted_at', null],
      ])
    ->get();    
      return $destinations;
  }
  public function showDestination($id)
  {
    //getdata single
    // $destinations = Destination::find($id);

    $destinations = DB::table('destinations')
    ->join('companies', 'destinations.company_code', '=', 'companies.company_code')
    ->where('destinations.id', '=',$id )
    ->select('code','location','name','company_name','destinations.id as id')
    ->get();

    if(!$destinations)
    abort(404);

    return $destinations;
  }

  public function storeDestination(Request $request)
  {
    //insert mass assginment
    $data = Destination::create([
      'code'        => $request->code,
      'name'        => $request->name,
      'location'    => $request->location,
      'company_code'=> $request->company_code,
    ]);

    $id = $data->id;
    $cc = $request->company_code;

    $warehouse = DB::table('warehouses')
    ->where([
      ['company_code', $cc],
      ['deleted_at', null],
      ])
    ->get();

    $count = count($warehouse);
    if ($count > 0){
      foreach ($warehouse as $key) {
        // $idd = $key->id;
        Cost::create([
          'id_warehouse'          => $key->id,
          'id_destination'        => $id,
          'type'                  => 'normal',
          'moda'                  => 'truck',
          'cost'                  => 30,
          'company_code'          => $request->company_code,
          ]);
        }
      }

    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function updateStatusDestination(Request $request, $id)
  {
    // update mass assginment
    Destination::where('id', $id)->update([
      'status'    => $request->status,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function updateDestination(Request $request, $id)
  {
    // update mass assginment
    Destination::where('id', $id)->update([
      'code'        => $request->code,
      'name'        => $request->name,
      'location'    => $request->location,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function destroyDestination($id)
  {
    // delete dengan fungsi softdelete
    Destination::find($id)->delete();
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }
//----------------------------------- Crud Transactions -----------------------------------------------//
  public function getTransaction($cc)
  //getdata all
  {
    $transactions = DB::table('transactions')
    ->join('companies', 'transactions.company_code', '=', 'companies.company_code')
    ->join('warehouses', 'transactions.id_warehouse', '=', 'warehouses.id')
    ->join('destinations', 'transactions.id_destination', '=', 'destinations.id')
    ->where([
      ['transactions.company_code', $cc],
      ['transactions.deleted_at', null],
      ])
    ->select('transactions.id','transaction_code','id_warehouse','id_destination','total_cost', 'total_demand', 'transactions.status', 'transactions.company_code', 'date', 'companies.company_name', 'warehouses.name as warehouse_name', 'destinations.name as destination_name')
    ->get();
    // ->toArray();

    $warehouse_id = [];
    foreach ($transactions as $key=>$row) {
      $temp_w = [];
      $implode_arr = (explode(',',$row->id_warehouse));
      foreach ($implode_arr as $row_id) {
        $temp_warehouse_id = DB::table('warehouses')
        ->select('name')
        ->where([
          ['id', $row_id],
          ])
          ->get()
          ->toArray();

        array_push($temp_w, ...$temp_warehouse_id);
      }
      array_push($warehouse_id, $temp_w);
    }

    $destination_id = [];
    foreach ($transactions as $key=>$row) {
      $temp_w = [];
      $implode_arr = (explode(',',$row->id_destination));
      foreach ($implode_arr as $row_id) {
        $temp_destination_id = DB::table('destinations')
        ->select('name')
        ->where([
          ['id', $row_id],
          ])
          ->get()
          ->toArray();

        array_push($temp_w, ...$temp_destination_id);
      }
      array_push($destination_id, $temp_w);
    }


    $results = [
      'transactions' => $transactions,
      'warehouses' => $warehouse_id,
      'destinations' => $destination_id,
    ];
    if(!$transactions)
    abort(404);

    return $results;
  }

  public function showTransaction($id)
  {
    //getdata single
    $transactions = Transaction::find($id);

    if(!$transactions)
    abort(404);

    return $transactions;
  }

  public function storeTransaction(Request $request)
  {
    //insert mass assginment
    Transaction::create([
      'id_warehouse'      => $request->id_warehouse,
      'transaction_code'  => 'TC-'.$request->company_code.'-'.rand(001,999),
      'id_destination'    => $request->id_destination,
      'total_cost'        => $request->total_costs * 1000,
      'total_demand'      => $request->total_demand,
      'company_code'      => $request->company_code,
      'status'            => 'pending',
      'date'              => date('Y-m-d'),
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function updateTransaction(Request $request, $id)
  {
    // update mass assginment
    Transaction::where('id', $id)->update([
      'status'    => $request->status,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function destroyTransaction($id)
  {
    // delete dengan fungsi softdelete
    Transaction::find($id)->delete();
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }
//-------------------------------- Crud Costs ------------------------------------------------------------//
  public function getCost($cc)
  //getdata all
  {
    $costs = DB::table('costs')
    ->join('companies', 'costs.company_code', '=', 'companies.company_code')
    ->join('warehouses', 'costs.id_warehouse', '=', 'warehouses.id')
    ->join('destinations', 'costs.id_destination', '=', 'destinations.id')
    ->where([
      ['costs.company_code', $cc],
      ['costs.deleted_at', null],
      ])
    ->select('costs.id','id_warehouse','id_destination','type', 'moda', 'cost', 'costs.company_code','warehouses.name as warehouse_name','destinations.name as destination_name')
    ->get();

    if(!$costs)
    abort(404);

    return $costs;
  }

  public function showCost($id)
  {
    //getdata single
    $costs = DB::table('costs')
    ->join('companies', 'costs.company_code', '=', 'companies.company_code')
    ->join('warehouses', 'costs.id_warehouse', '=', 'warehouses.id')
    ->join('destinations', 'costs.id_destination', '=', 'destinations.id')
    ->where([
      ['costs.id', $id],
      ])
    ->select('costs.id','id_warehouse','id_destination','type', 'moda', 'cost', 'costs.company_code','warehouses.name as warehouse_name','destinations.name as destination_name')
    ->get();

    if(!$costs)
    abort(404);

    return $costs;
  }

  public function storeCost(Request $request)
  {
    //insert mass assginment
    Cost::create([
      'id_warehouse'    => $request->id_warehouse,
      'id_destination'  => $request->id_destination,
      'type'            => $request->type,
      'cost'            => $request->cost,
      'company_code'    => $request->company_code,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function updateCost(Request $request, $id)
  {
    // update mass assginment
    Cost::where('id', $id)->update([
      'moda'            => $request->moda,
      'cost'            => $request->cost,
    ]);
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function destroyCost($id)
  {
    // delete dengan fungsi softdelete
    Cost::find($id)->delete();
    $myArr = array(
      "status" => true,
    );
    return json_encode($myArr);
  }

  public function getGraph($cc,$y)
  //getdata all
  {
    // print_r($y); die;
    $graph = DB::select(
      "SELECT COUNT(id) as jml,monthname(date) as bln FROM transactions where company_code = '".$cc."' and year(date) = '".$y."' GROUP BY month(date)"
      // "SELECT COUNT(id) as jml,monthname(date) as bln FROM transactions where company_code = '".$cc."' GROUP BY month(date)"
    );
    
    // dd($graph);
    // print_r($cc); die;
    return $graph;


    }
}
