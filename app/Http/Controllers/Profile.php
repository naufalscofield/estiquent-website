<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class Profile extends Controller
{
    public function index()
    {
        $id = session('id_user');
        $pw = session('password');
        $role = session('role');
        // dd($role);
        return view('admin/profile',["id"=>$id,"pw"=>$pw,"role"=>$role]);
    }
}
