<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('login');
});
Route::group(['middleware' => 'StoreSession'],function(){
    // Route::get('/', 'Profile@index');
    Route::get('/admin/{year?}', 'Admin@index');
    Route::get('/profile', 'Profile@index');
    Route::get('/warehouses', 'Warehouses@index');
    Route::post('/warehouses/tes', 'Warehouses@tes');
    Route::get('/destinations', 'Destinations@index');
    Route::get('/transactions', 'Transactions@index');
    Route::get('/costs', 'Costs@index');
    Route::get('/company', 'Company@index');
    Route::get('/logs', 'Logs@index');
    Route::get('/users', 'Users@index');
    Route::get('/nwc', 'NWC@index');
});
Route::get('/register', 'Register@index');
Route::get('/session/{id}/{pw}/{r}', 'Session@index');
Route::get('/logout', 'Session@logout');
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Route::get('/blog', 'BlogController@index');

// Route::get('/blog/create', 'BlogController@create');
// Route::post('/blog', 'BlogController@store');

// Route::get('/blog/{id}', 'BlogController@show');

// Route::get('/blog/{id}/edit', 'BlogController@edit');
// Route::put('/blog/{id}', 'BlogController@update');

// Route::delete('/blog/{id}', 'BlogController@destroy');
