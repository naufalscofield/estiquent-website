-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2019 at 03:51 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_estiquent`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `short_name` varchar(10) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL,
  `email` varchar(20) NOT NULL,
  `logo` text NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `short_name`, `company_code`, `address`, `description`, `email`, `logo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PT.Sukarasa Berjaya Makmur', 'sukarasa', 'est_sukarasa_794', 'Jln.Sukarasa', 'ini perusahaan logistik', 'ramnaufal@gmail.com', '$request->logo', '2018-12-07', '2018-12-07', NULL),
(2, 'CV.Niftra Soft', 'niftra', 'est_niftra_445', 'Jln.Sucore', 'ini cv software', 'niftra@niftra', '$request->logo', '2018-12-07', '2018-12-07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `costs`
--

CREATE TABLE `costs` (
  `id` int(11) NOT NULL,
  `id_warehouse` int(11) NOT NULL,
  `id_destination` int(11) NOT NULL,
  `type` enum('normal','fast') NOT NULL,
  `moda` enum('truck','ship','plane') NOT NULL,
  `cost` int(11) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costs`
--

INSERT INTO `costs` (`id`, `id_warehouse`, `id_destination`, `type`, `moda`, `cost`, `company_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'normal', 'truck', 90, 'est_sukarasa_794', NULL, NULL, NULL),
(2, 1, 2, 'normal', 'plane', 60, 'est_sukarasa_794', NULL, NULL, NULL),
(6, 2, 1, 'normal', 'plane', 80, 'est_sukarasa_794', NULL, NULL, NULL),
(7, 2, 2, 'normal', 'plane', 60, 'est_sukarasa_794', NULL, NULL, NULL),
(8, 5, 1, 'normal', 'plane', 30, 'est_sukarasa_794', NULL, NULL, NULL),
(9, 5, 2, 'normal', 'plane', 35, 'est_sukarasa_794', NULL, NULL, NULL),
(10, 1, 7, 'normal', 'plane', 10, 'est_sukarasa_794', NULL, NULL, NULL),
(11, 2, 7, 'normal', 'plane', 15, 'est_sukarasa_794', NULL, NULL, NULL),
(12, 5, 7, 'normal', 'plane', 31, 'est_sukarasa_794', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(20) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`id`, `code`, `name`, `location`, `company_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'd01', 'destination A', 'cikampek', 'est_sukarasa_794', '2018-12-08', '2018-12-08', NULL),
(2, 'd02', 'destination B', 'pangalengan', 'est_sukarasa_794', '2018-12-08', '2018-12-08', NULL),
(5, 't01', 'tujuan A', 'garut', 'est_niftra_445', '2018-12-09', '2018-12-09', NULL),
(6, 't02', 'tujuan B', 'bogor', 'est_niftra_445', '2018-12-09', '2018-12-09', '2018-12-09'),
(7, 'd03', 'destination C', 'Banten', 'est_sukarasa_794', '2018-12-08', '2018-12-08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `activity` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `transaction_code` varchar(30) NOT NULL,
  `id_warehouse` text NOT NULL,
  `id_destination` text NOT NULL,
  `total_cost` int(11) NOT NULL,
  `total_demand` int(11) NOT NULL,
  `status` enum('pending','approved','rejected') NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transaction_code`, `id_warehouse`, `id_destination`, `total_cost`, `total_demand`, `status`, `company_code`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'tcniftra568', '3', '5', 1900000, 170, 'rejected', 'est_niftra_445', '2018-12-14', '2018-12-10', '2018-12-09', NULL),
(2, 'tcsukarasa999', '1', '1', 2700000, 200, 'pending', 'est_sukarasa_794', '2018-12-06', '2018-12-10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `birth_place` varchar(50) NOT NULL,
  `birth_date` date NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `email` varchar(25) NOT NULL,
  `photo` text,
  `gender` enum('male','female','undefined') NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `birth_place`, `birth_date`, `username`, `password`, `role`, `email`, `photo`, `gender`, `company_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'wa', 'wa', 'wa', '1999-02-20', 'wa', 'c68c559d956d4ca20f435ed74a6e71e6', 'admin', 'w@wa', 'default.jpg', 'undefined', 'est_sukarasa_794', '2018-12-07', '2018-12-07', NULL),
(2, 'farhan', 'farhan', 'farhan', '2018-12-04', 'farhan', 'd1bbb2af69fd350b6d6bd88655757b47', 'user', 'pemilik@gmail.com', 'default.jpg', 'male', 'est_sukarasa_794', '2018-12-07', '2018-12-07', NULL),
(3, 'Jane', 'Doe', 'Earth', '2018-12-07', 'est_niftra_445', '6aa2be92bdb56c28649a9a40d3c3d044', 'admin', 'niftra@niftra', 'default.jpg', 'undefined', 'est_niftra_445', '2018-12-07', '2018-12-07', NULL),
(4, 'nif', 'nif', 'nif', '2018-12-04', 'nif', '1f5ce3fc856f5e5bb2cda200f902e63d', 'user', 'visca@outlook.com', 'default.jpg', 'male', 'est_niftra_445', '2018-12-07', '2018-12-07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `capacity` int(11) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `code`, `name`, `location`, `capacity`, `company_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'w01', 'warehouse A', 'bandung', 10, 'est_sukarasa_794', '2018-12-08', '2018-12-08', NULL),
(2, 'w02', 'warehouse B', 'soreang', 41, 'est_sukarasa_794', '2018-12-08', '2018-12-08', '2018-12-08'),
(3, 'g01', 'gudang A', 'bandung', 80, 'est_niftra_445', '2018-12-08', '2018-12-08', NULL),
(4, 'g02', 'gudang B', 'cicalengka', 90, 'est_niftra_445', '2018-12-08', '2018-12-08', '2018-12-08'),
(5, 'w03', 'warehouse C', 'soreang', 20, 'est_sukarasa_794', '2018-12-08', '2018-12-08', '2018-12-08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_code_2` (`company_code`),
  ADD KEY `company_code` (`company_code`);

--
-- Indexes for table `costs`
--
ALTER TABLE `costs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_destination` (`id_destination`),
  ADD KEY `id_warehouse` (`id_warehouse`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_code` (`company_code`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_code` (`company_code`),
  ADD KEY `company_code_2` (`company_code`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_code` (`company_code`),
  ADD KEY `company_code_2` (`company_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `costs`
--
ALTER TABLE `costs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `costs`
--
ALTER TABLE `costs`
  ADD CONSTRAINT `costs_ibfk_2` FOREIGN KEY (`id_destination`) REFERENCES `destinations` (`id`);

--
-- Constraints for table `destinations`
--
ALTER TABLE `destinations`
  ADD CONSTRAINT `destinations_ibfk_1` FOREIGN KEY (`company_code`) REFERENCES `companies` (`company_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company_code`) REFERENCES `companies` (`company_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD CONSTRAINT `warehouses_ibfk_1` FOREIGN KEY (`company_code`) REFERENCES `companies` (`company_code`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
